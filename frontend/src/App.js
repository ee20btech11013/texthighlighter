import React from "react";
import { Routes, Route } from "react-router-dom";
import Addtext from "./pages/Addtext";
import Home from "./pages/Home";
import "./index.css";
import { BrowserRouter } from "react-router-dom";
import "./index.css";
function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="addText" element={<Addtext />} />
      </Routes>
    </BrowserRouter>
  );
}
export default App;
