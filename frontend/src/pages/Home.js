import Leftsidebar from "../Components/Leftsidebar";
import Middleview from "../Components/Middleview";
import Rightsidebar from "../Components/Rightsidebar";
import { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import Text from "../Components/text";
function Home() {
  const [middletext, setmiddletext] = useState("");
  const [selectedtext, setselectedtext] = useState(null);
  const [selectedtextstart, setselectedtextstart] = useState(0);
  const [selectedtextend, setselectedtextend] = useState(0);
  const [text, settext] = useState(Text);
  const [category, setcategory] = useState("");
  const color = {
    "Category 1": "#EB1F1F",
    "Category 2": "#37A794",
    "Category 3": "#01354B",
    "Category 4": "#F69603",
    "Category 5": "#54B9F1",
    "Category 6": "#567DF3",
    "Category 7": "#20C73A",
    "Category 8": "#843B06",
  };
  // useEffect(() => {
  //   // Get initial all text data from database for first time page loads
  //   // We can add this later once we done with backend
  //   fetch("http://127.0.0.1:5000/api/gettextdata", {
  //     method: "GET",
  //   }).then((res) => {
  //     if (res.ok) {
  //       res.json().then((ret) => settext(ret));
  //     }
  //   });
  // }, []);
  const [num, setnum] = useState(null);
  return (
    <div className="bg-[#F7F7FB] w-[100vw] h-[100vh]">
      <div className="Home mx-[2vw] bg-[#F7F7FB]">
        {/* <Link
        to="/addText"
        class="bg-blue-500 hover:bg-blue-700 z-[1000] text-white
       font-bold py-2 px-4 rounded absolute bottom-2 right-2 font-robo"
      >
        Add text into DB
      </Link> */}
        <div className="bg-white overflow-y-auto absolute top-[4vh] left-[2vw] w-[18vw] text-black h-[92vh]">
          <Leftsidebar
            text={text}
            num={num}
            middletext={middletext}
            setnum={setnum}
            setmiddletext={setmiddletext}
          />
        </div>
        <div
          className="bg-white font-robo overflow-y-auto top-[4vh] absolute left-[20vw] w-[52vw] h-[92vh]"
          style={{
            border: "1px solid #D8DCE9",
            borderTopRightRadius: "10px",
            borderBottomRightRadius: "10px",
          }}
        >
          <Middleview
            middletext={middletext}
            num={num}
            selectedtextstart={selectedtextstart}
            selectedtextend={selectedtextend}
            setselectedtext={setselectedtext}
            setselectedtextend={setselectedtextend}
            selectedtext={selectedtext}
            setselectedtextstart={setselectedtextstart}
            category={category}
            color={color}
          />
        </div>
        <div
          className="bg-white overflow-y-auto absolute top-[4vh] right-[2vw] w-[25vw] h-[92vh]"
          style={{
            border: "1px solid #D8DCE9",
            borderTopRightRadius: "10px",
            borderTopLeftRadius: "10px",
            borderBottomRightRadius: "10px",
            borderBottomLeftRadius: "10px",
          }}
        >
          <Rightsidebar
            num={num}
            selectedtext={selectedtext}
            category={category}
            setcategory={setcategory}
          />
        </div>
      </div>
    </div>
  );
}

export default Home;
