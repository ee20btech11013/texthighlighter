import React, { useEffect, useState } from "react";
import "../index.css";
import left from "../Images/left.svg";
import right from "../Images/right.svg";
import styles from "../css/Leftsidebar.module.css";
function Leftsidebar({ text, num, middletext, setnum, setmiddletext }) {
  const pagination_num = 11;
  const Indexes = [];
  const [sliceIndex, setsliceIndex] = useState([0, pagination_num]);
  const [sliceIndex_bottom, setsliceIndexBottom] = useState([
    0,
    Math.min(3, text.length),
  ]);
  for (let i = 0; i < Math.ceil(text.length / pagination_num); i++) {
    if (i + 1 < 10) {
      Indexes.push(`0${i + 1}`);
    } else {
      Indexes.push(`${i + 1}`);
    }
  }
  useEffect(() => {
    setmiddletext("");
    setnum(null);
  }, [sliceIndex_bottom[0]]);
  return (
    <div
      className="sidebar overflow-hidden min-h-[92vh]"
      style={{
        border: "1px solid #D8DCE9",
        borderTopLeftRadius: "10px",
        borderBottomLeftRadius: "10px",
      }}
    >
      <div
        className="py-3 ml-[4vw] bg-white font-extrabold leading-[23px]
        text-[#4B4B4B] font-robo text-[18px]"
      >
        Settings
      </div>
      <hr />
      <input
        type="text"
        name="search"
        id="search"
        className={`${styles.searchicon} mt-4 border-[1px] border-[#D8DCE9] rounded-[4px] w-[90%] ml-[5%] outline-none text-[#4B4B4B]`}
      />
      <div className="h-[66vh] pt-4 flex flex-col justify-start">
        {text.slice(sliceIndex[0], sliceIndex[1]).map((text, index) => (
          <div
            key={index}
            className={`${
              num === index + sliceIndex[0] ? "bg-[#F6F7FA]" : ""
            } font-robo font-normal cursor-pointer w-[90%] px-4 py-1 mx-2`}
            onClick={() => {
              setnum(index + sliceIndex[0]);
              setmiddletext(text["text"]);
            }}
            style={{ borderRadius: "8px" }}
          >
            <span className="font-bold">
              {sliceIndex[1] / pagination_num < 10
                ? `0${sliceIndex[1] / pagination_num}`
                : `${sliceIndex[1] / pagination_num}`}
            </span>{" "}
            &nbsp; {text["name"]}
          </div>
        ))}
      </div>
      <div
        className="h-[6vh] m-2 border-2 bg-[#E7F1F2]"
        style={{ borderRadius: "0px 0px 8px 8px" }}
      >
        <div className="flex justify-between w-[80%] innerbox mx-4 my-[0.5vh] h-[4vh]">
          <span
            onClick={() =>
              setsliceIndexBottom((prev) =>
                prev[1] < Math.ceil(text.length / pagination_num) - 3
                  ? [0, Math.min(text.length + 1, 3)]
                  : [prev[0] - 3, prev[1] - 3]
              )
            }
          >
            <img
              src={left}
              alt="left"
              className="py-1 px-[0.4em] bg-white cursor-pointer"
              style={{
                borderRadius: "4px",
                transform: "scale(0.8)",
                transform: "center center",
              }}
            />
          </span>
          <div className="flex flex-row justify-around">
            {Indexes.slice(sliceIndex_bottom[0], sliceIndex_bottom[1]).map(
              (item, index) => (
                <span
                  className="px-1 text-[14px] cursor-pointer"
                  key={index}
                  onClick={() =>
                    setsliceIndex([
                      (parseInt(item) - 1) * pagination_num,
                      parseInt(item) * pagination_num,
                    ])
                  }
                >
                  {item}
                </span>
              )
            )}
            {Indexes.length >= 3 ? "..." : ""}
          </div>
          <span
            onClick={() =>
              setsliceIndexBottom((prev) =>
                prev[1] > Math.ceil(text.length / pagination_num) - 3
                  ? [
                      Math.ceil(text.length / pagination_num) - 3,
                      Math.ceil(text.length / pagination_num),
                    ]
                  : [prev[0] + 3, prev[1] + 3]
              )
            }
          >
            <img
              src={right}
              alt="right"
              className="py-1 px-[0.4em] bg-white cursor-pointer"
              style={{
                borderRadius: "4px",
                transform: "scale(0.8)",
                transform: "center center",
              }}
            />
          </span>
        </div>
      </div>
    </div>
  );
}

export default Leftsidebar;
