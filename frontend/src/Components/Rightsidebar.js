import React, { useEffect, useState } from "react";

function Rightsidebar({ num, selectedtext, category, setcategory }) {
  return (
    <>
      <div
        className="py-3 ml-[2vw] bg-white font-extrabold leading-[23px]
        text-[#4B4B4B] font-robo text-[18px]"
      >
        Labels
      </div>
      <hr />
      <div className="m-2 flex flex-wrap text-[12px]">
        <button
          className={`${
            category === "Category 1" ? "bg-[#EB1F1F] text-white" : ""
          } h-8 px-2 ${
            category === "Category 1" ? "text-white" : "text-[#EB1F1F]"
          } transition-colors duration-150 border border-[#EB1F1F] rounded-md focus:shadow-outline hover:bg-[#EB1F1F] hover:text-white m-1 font-robo`}
          onClick={() => setcategory("Category 1")}
        >
          Category 1
        </button>
        <button
          className={`${
            category === "Category 2" ? "bg-[#37A794] text-white" : ""
          } h-8 px-2 ${
            category === "Category 2" ? "text-white" : "text-[#37A794]"
          } transition-colors duration-150 border border-[#37A794] rounded-md focus:shadow-outline hover:bg-[#37A794] hover:text-white m-1 font-robo`}
          onClick={() => setcategory("Category 2")}
        >
          Category 2
        </button>
        <button
          className={`${
            category === "Category 3" ? "bg-[#01354B] text-white" : ""
          } h-8 px-2 ${
            category === "Category 3" ? "text-white" : "text-[#01354B]"
          } transition-colors duration-150 border border-[#01354B] rounded-md focus:shadow-outline hover:bg-[#01354B] hover:text-white m-1 font-robo`}
          onClick={() => setcategory("Category 3")}
        >
          Category 3
        </button>
        <button
          className={`${
            category === "Category 4" ? "bg-[#F69603] text-white" : ""
          } h-8 px-2 ${
            category === "Category 4" ? "text-white" : "text-[#F69603]"
          } transition-colors duration-150 border border-[#F69603] rounded-md focus:shadow-outline hover:bg-[#F69603] hover:text-white m-1 font-robo`}
          onClick={() => setcategory("Category 4")}
        >
          Category 4
        </button>
        <button
          className={`${
            category === "Category 5" ? "bg-[#54B9F1] text-white" : ""
          } h-8 px-2 ${
            category === "Category 5" ? "text-white" : "text-[#54B9F1]"
          } transition-colors duration-150 border border-[#54B9F1] rounded-md focus:shadow-outline hover:bg-[#54B9F1] hover:text-white m-1 font-robo`}
          onClick={() => setcategory("Category 5")}
        >
          Category 5
        </button>
        <button
          className={`${
            category === "Category 6" ? "bg-[#567DF3]" : ""
          } h-8 px-2 ${
            category === "Category 6" ? "text-white" : "text-[#567DF3]"
          } transition-colors duration-150 border border-[#567DF3] rounded-md focus:shadow-outline hover:bg-[#567DF3] hover:text-white m-1 font-robo`}
          onClick={() => setcategory("Category 6")}
        >
          Category 6
        </button>
        <button
          className={`${
            category === "Category 7" ? "bg-[#20C73A] text-white" : ""
          } h-8 px-2 ${
            category === "Category 7" ? "text-white" : "text-[#20C73A]"
          } transition-colors duration-150 border border-[#20C73A] rounded-md focus:shadow-outline hover:bg-[#20C73A] hover:text-white m-1 font-robo`}
          onClick={() => setcategory("Category 7")}
        >
          Category 7
        </button>
        <button
          className={`${
            category === "Category 8" ? "bg-[#843B06] text-white" : ""
          } h-8 px-2 ${
            category === "Category 8" ? "text-white" : "text-[#843B06]"
          } transition-colors duration-150 border border-[#843B06] rounded-md focus:shadow-outline hover:bg-[#843B06] hover:text-white m-1 font-robo`}
          onClick={() => setcategory("Category 8")}
        >
          Category 8
        </button>
      </div>
    </>
  );
}

export default Rightsidebar;
