import React, { useEffect, useState } from "react";
function Middleview({
  middletext,
  num,
  category,
  setselectedtext,
  selectedtext,
  setselectedtextstart,
  setselectedtextend,
  selectedtextend,
  selectedtextstart,
  color,
}) {
  const [selectedlocal, setselectedlocal] = useState([]);
  const handleselect = () => {
    if (category != "") {
      if (window.getSelection().toString != "") {
        setselectedtext(window.getSelection().toString());
        setselectedtextstart(window.getSelection().getRangeAt(0).startOffset);
        setselectedtextend(window.getSelection().getRangeAt(0).endOffset);
        setselectedlocal((prev) =>
          selectedtext !== null || selectedtext !== ""
            ? [
                ...prev,
                {
                  text: selectedtext,
                  start: selectedtextstart,
                  end: selectedtextend,
                  category: category,
                  num: num,
                },
              ]
            : prev
        );
        localStorage.setItem("localdata", JSON.stringify(selectedlocal));
      }
    } else {
      alert("Please select a category from the Right side labels section");
    }
  };
  useEffect(() => {
    setselectedtextstart(0);
    setselectedtextend(0);
    setselectedtext("");
  }, [num]);
  return (
    <div>
      <div className="font-robo font-mediumbold m-4 leading-[25px]">
        {middletext ? (
          <>
            {selectedtextstart === selectedtextend ? (
              <span onMouseUp={handleselect}>{middletext}</span>
            ) : (
              <>
                <span id="middletext" onMouseUp={handleselect}>
                  {middletext.slice(
                    0,
                    Math.min(selectedtextstart, selectedtextend)
                  )}
                </span>
                <span
                  className={`leading-[45px] border-2 rounded-sm border-[${color[category]}] pb-4 pt-2 relative`}
                >
                  {middletext.slice(
                    Math.min(selectedtextstart, selectedtextend),
                    Math.max(selectedtextstart, selectedtextend)
                  )}
                  <div
                    className={`absolute py-1 left-0 w-[100%] bottom-0 text-center text-[10px] bg-[${color[category]}] leading-[10px] text-white`}
                  >
                    {selectedtext.length < 7
                      ? `${category.slice(0, 3)}${category.slice(
                          category.length - 2,
                          category.length
                        )}`
                      : category}
                  </div>
                  <div
                    className={`absolute top-[-5px] w-[10px] right-[-5px] leading-[10px] text-white rounded-[10px] bg-[${color[category]}] cursor-pointer text-[10px] text-center`}
                  >
                    x
                  </div>
                </span>
                <span id="middletext" onMouseUp={handleselect}>
                  {middletext.slice(
                    Math.max(selectedtextstart, selectedtextend),
                    middletext.length
                  )}
                </span>
              </>
            )}
          </>
        ) : (
          ""
        )}
      </div>
    </div>
  );
}

export default Middleview;
